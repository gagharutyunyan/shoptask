@extends('layouts.app')

@section('search')
    @include('layouts.partials.search', ['category' => $category, 'route' => '?'])
@endsection

@section('content')

{{--    @if ($categories)--}}
{{--        <div class="card card-default mb-3">--}}
{{--            <div class="card-header">--}}
{{--                @if ($category)--}}
{{--                    Categories of {{ $category->name }}--}}
{{--                @else--}}
{{--                    Categories--}}
{{--                @endif--}}
{{--            </div>--}}
{{--            <div class="card-body pb-0" style="color: #aaa">--}}
{{--                <div class="row">--}}
{{--                    @foreach (array_chunk($categories, 3) as $chunk)--}}
{{--                        <div class="col-md-3">--}}
{{--                            <ul class="list-unstyled">--}}
{{--                                @foreach ($chunk as $current)--}}
{{--                                    <li>--}}
{{--                                        <a href="{{ route('adverts.index', array_merge(['adverts_path' => adverts_path($region, $current)], request()->all())) }}">{{ $current->name }}</a>--}}
{{--                                        ({{ $categoriesCounts[$current->id] ?? 0 }})--}}
{{--                                    </li>--}}
{{--                                @endforeach--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    @endforeach--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    @endif--}}

    @if ($regions)
        <div class="card card-default mb-3">
            <div class="card-header">
                @if ($region)
                    Regions of {{ $region->name }}
                @else
                    Regions
                @endif
            </div>
            <div class="card-body pb-0" style="color: #aaa">
                <div class="row">
                    @foreach (array_chunk($regions, 3) as $chunk)
                        <div class="col-md-3">
                            <ul class="list-unstyled">
                                @foreach ($chunk as $current)
                                    <li>
                                        <a href="{{ route('adverts.index', array_merge(['adverts_path' => adverts_path($current, $category)], request()->all())) }}">{{ $current->name }}</a>
                                        ({{ $regionsCounts[$current->id] ?? 0 }})
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @endif

    <div class="row">
        <aside class="col-sm-4">
            <div class="card">
                <article class="card-group-item">
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown"> Categories  </a>
                            <ul class="dropdown-menu">
                                @foreach($catMenu as $menu)
                                    <li><a class="dropdown-item" href="{{ route('adverts.index', array_merge(['adverts_path' => adverts_path($region, $menu)], request()->all())) }}"> {{ $menu->name }}</a>
                                        @if($menu->children->isNotEmpty())
                                            <ul class="submenu dropdown-menu">
                                                @foreach($menu->children as $child)
                                                    <li><a class="dropdown-item" href="{{ route('adverts.index', array_merge(['adverts_path' => adverts_path($region, $child)], request()->all())) }}">{{$child->name}}</a></li>
                                                @endforeach
                                            </ul>
                                        @endif
                                    </li>
                                @endforeach
                            </ul>
                        </li>
                    </ul>
                </article>
                <article class="card-group-item">
                    <header class="card-header">
                        <h6 class="title">Range input </h6>
                    </header>
                    <div class="filter-content">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Min</label>
                                    <input type="number" class="form-control" placeholder="$0">
                                </div>
                                <div class="form-group col-md-6 text-right">
                                    <label>Max</label>
                                    <input type="number" class="form-control" placeholder="$1,0000">
                                </div>
                            </div>
                        </div> <!-- card-body.// -->
                    </div>
                </article> <!-- card-group-item.// -->
                <form action="?" method="GET">
                <article class="card-group-item">
                    <header class="card-header">
                        <h6 class="title">Selection </h6>
                    </header>
                        @if ($category)
                    <div class="filter-content">
                        @foreach ($category->allAttributes() as $attribute)
                            @if ($attribute->isSelect() || $attribute->isNumber())
                        <div class="card-body">
                            <div class="form-group">
                                <label class="col-form-label">{{ $attribute->name }}</label>

                                @if ($attribute->isSelect())
                                    <select class="form-control" name="attrs[{{ $attribute->id }}][equals]">
                                        <option value=""></option>
                                        @foreach ($attribute->variants as $variant)
                                            <option
                                                value="{{ $variant }}"{{ $variant === request()->input('attrs.' . $attribute->id . '.equals') ? ' selected' : '' }}>
                                                {{ $variant }}
                                            </option>
                                        @endforeach
                                    </select>

                                @elseif ($attribute->isNumber())
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="number" class="form-control"
                                                   name="attrs[{{ $attribute->id }}][from]"
                                                   value="{{ request()->input('attrs.' . $attribute->id . '.from') }}"
                                                   placeholder="From">
                                        </div>
                                        <div class="col-md-6">
                                            <input type="number" class="form-control"
                                                   name="attrs[{{ $attribute->id }}][to]"
                                                   value="{{ request()->input('attrs.' . $attribute->id . '.to') }}"
                                                   placeholder="To">
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div> <!-- card-body.// -->
                            @endif
                        @endforeach
                            <div class="form-group">
                                <button class="btn btn-light border" type="submit"><span
                                        class="fa fa-search"></span></button>
                            </div>
                    </div>
                            @endif
                </article> <!-- card-group-item.// -->
                </form>

            </div> <!-- card.// -->
        </aside> <!-- col.// -->
        <div class="col-md-8">
            <div class="adverts-list">
                @foreach ($adverts as $advert)
                    <div class="advert">
                        <div class="row">
                            <div class="col-md-3">
                                <div style="height: 180px; background: #f6f6f6; border: 1px solid #ddd"></div>
                            </div>
                            <div class="col-md-9">
                                <span class="float-right">{{ $advert->price }}</span>
                                <div class="h4" style="margin-top: 0"><a href="{{ route('adverts.show', $advert) }}">{{ $advert->title }}</a></div>
                                <p>Region: <a href="">{{ $advert->region ? $advert->region->name : 'All' }}</a></p>
                                <p>Category: <a href="">{{ $advert->category->name }}</a></p>
                                <p>Date: {{ $advert->created_at }}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            {{ $adverts->links() }}
        </div>

    </div>

@endsection
@section('scripts')
    <script type="text/javascript">
        /// some script

        // jquery ready start
        $(document).ready(function() {
            // jQuery code

            //////////////////////// Prevent closing from click inside dropdown
            $(document).on('click', '.dropdown-menu', function (e) {
                e.stopPropagation();
            });

            // make it as accordion for smaller screens
            if ($(window).width() < 992) {
                $('.dropdown-menu a').click(function(e){
                    e.preventDefault();
                    if($(this).next('.submenu').length){
                        $(this).next('.submenu').toggle();
                    }
                    $('.dropdown').on('hide.bs.dropdown', function () {
                        $(this).find('.submenu').hide();
                    })
                });
            }

        }); // jquery end
    </script>
@endsection
