<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\UseCases\Auth\RegisterService;

class RegisterController extends Controller
{
    private $service;

    /**
     * Create a new controller instance.
     *
     * @param RegisterService $service
     */
    public function __construct(RegisterService $service)
    {
        $this->middleware('guest');
        $this->service = $service;
    }

    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    protected function create(RegisterRequest $request)
    {
        $this->service->register($request);

        return redirect()->route('login')
            ->with('success', 'Check your email and click on the link to verify.');

    /*
    Crutch
    Expected response code 354 but got code "503", with message "503 Valid RCPT command must precede DATA "

            try {
            $this->service->register($request);
            return redirect()->route('login')
                ->with('success', 'Check your email and click on the link to verify.');
        } catch (Swift_TransportException $e) {
            if ($e->getMessage()) {
                DB::table('users')->where('email', '=', $request->email)->take(1)->delete();
                return redirect()->route('register')->with('error', 'This E-mail is not valid, please try another one.');
            }
        }
    */
    }

    public function verify($token){
        if (!$user = User::where('verify_token', $token)->first()) {
            return redirect()->route('login')
                ->with('error', 'Sorry your link cannot be identified.');
        }
        try{
            $this->service->verify($user->id);
            return redirect()->route('login')
                ->with('success', 'Your e-mail is verified. You can now login.');
        } catch (\DomainException $e){
            return redirect()->route('login')->with('error', $e->getMessage());
        }
    }
}
