<?php

namespace App\Http\Controllers\Adverts;

namespace App\Http\Controllers\Adverts;

use App\Http\Requests\Adverts\SearchRequest;
use App\Http\Router\AdvertsPath;
use App\Models\Adverts\Advert\Advert;
use App\Models\Adverts\Category;
use App\Models\Region;
use Kalnoy\Nestedset\QueryBuilder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class AdvertController extends Controller
{
    public function index(SearchRequest $request, AdvertsPath $path)
    {
        $query = Advert::active()->with(['category', 'region'])->orderByDesc('id');

        // Filter Request

        $arr = [];
        if(!empty($request->attrs)) {
            foreach ($request->attrs as $k => $v) {
                if ($v['equals'] != null) {
                    $arr[] = $v['equals'];
                    $query->leftJoin('advert_advert_values', 'advert_adverts.id', '=', 'advert_advert_values.advert_id')->whereIn('value', $arr);
                }
            }
        }

        if ($category = $path->category) {
            $query->forCategory($category);
        }

        if ($region = $path->region) {
            $query->forRegion($region);
        }

        $regions = $region
            ? $region->children()->orderBy('name')->getModels()
            : Region::roots()->orderBy('name')->getModels();

        $categories = $category
            ? $category->children()->orderBy('name')->getModels()
            : Category::whereIsRoot()->orderBy('name')->getModels();

        $adverts = $query->paginate(20);

        $catMenu = Category::get()->toTree();

//        dump($catMenu);
//        foreach ($catMenu as $menu){
//            for ($i = 0;$i < count($menu->children);$i++){
//                if($menu->children->isNotEmpty()) {
//                    dump($menu->children[$i]);
//                }
//            }
//        }

//        dd([
//            'category' => $category,
//            'region' => $region,
//            'adverts' => $adverts,
//            'catMenu' => $catMenu,
//            'regions' => $regions,
//            'categories' => $categories
//        ]);

        return view('adverts.index', compact(
            'category',
            'region',
            'adverts',
            'catMenu',
            'regions'
            //'categories'
        ));
    }

    public function show(Advert $advert)
    {
        if (!($advert->isActive() || Gate::allows('show-advert', $advert))) {
            abort(403);
        }

        $user = Auth::user();

        return view('adverts.show', compact('advert', 'user'));
    }
}
