<?php

namespace App\Console\Commands\User;

use App\Models\User;
use Illuminate\Console\Command;

class RoleCommand extends Command
{
    protected $signature = 'user:role {email} {role}';

    protected $description = 'Change User Role: Example(user:role example@gmail.com admin)';

    public function handle(): bool
    {
        $email = $this->argument('email');
        $role = $this->argument('role');

        if(!$user = User::where('email', $email)->first())
        {
            $this->error('Undefined User Whit Email ' . $email);
            return false;
        }

        try{
            $user->changeRole($role);
        }catch (\DomainException $e)
        {
            $this->error($e->getMessage());
            return false;
        }

        $this->info('Role Is Successfully Changed');
        return true;
    }
}
