<?php

namespace App\Console\Commands\User;

use App\Models\User;
use App\UseCases\Auth\RegisterService;
use Illuminate\Console\Command;

class VerifyCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:verify {email}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Write Email To Verify';

    private $service;

    /**
     * Create a new command instance.
     *
     * @param RegisterService $service
     */
    public function __construct(RegisterService $service)
    {
        parent::__construct();

        $this->service = $service;
    }

    /**
     * Verify User
     *
     * @return bool
     */
    public function handle(): bool
    {
        /** @var User $user */

        $email = $this->argument('email');

        if(!$user = User::where('email', $email)->first())
        {
            $this->error('Undefined user with email' . $email);
            return false;
        }

        try {
            $this->service->verify($user->id);
        } catch (\DomainException $e) {
            $this->info($e->getMessage());
            return false;
        }

        $this->info('User is suc. verify');
        return true;
    }
}
