<?php


namespace App\UseCases\Auth;

use App\Http\Requests\Auth\RegisterRequest;

use App\Mail\Auth\VerifyMail;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Support\Facades\DB;
use Swift_TransportException;


class RegisterService
{
    private $mailer;
    private $dispatcher;

    public function __construct(Mailer $mailer, Dispatcher $dispatcher)
    {
        $this->mailer = $mailer;
        $this->dispatcher = $dispatcher;
    }

    public function register(RegisterRequest $request)
    {
        $user = User::register(
            $request['name'],
            $request['email'],
            $request['password']
        );
            // Uncomment to send emails
            //$this->mailer->to($user->email)->send(new VerifyMail($user));
            //$this->dispatcher->dispatch(new Registered($user));
    }

    /**
     * @var User $user
     */
    public function verify($id)
    {
        $user = User::findOrFail($id);
        $user->verify();
    }
}
