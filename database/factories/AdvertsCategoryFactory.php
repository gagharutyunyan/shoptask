<?php

use Faker\Generator as Faker;
use Illuminate\Support\Str as StrAlias;
/** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(\App\Models\Adverts\Category::class, function (Faker $faker)
{
    return [
      'name' => $name = $faker->unique()->name,
      'slug' => StrAlias::slug($name,'-'),
      'parent_id' => null
    ];
});
