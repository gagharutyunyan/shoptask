<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use App\Models\Region;
use Faker\Generator as Faker;
use Illuminate\Support\Str as StrAlias;

$factory->define(Region::class, function (Faker $faker) {
    return [
        'name' => $name = $faker->unique()->city,
        'slug' => StrAlias::slug($name,'-'),
        'parent_id' => null
    ];
});
